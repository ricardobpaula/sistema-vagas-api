require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const path = require('path');
const socketio = require('socket.io');
const http = require('http');

const routes = require('./routes');

const app = express();
const server = http.Server(app);
const io = socketio(server);

mongoose.connect(
  process.env.MONGO_URL,
  { useNewUrlParser: true, useFindAndModify: true, useUnifiedTopology: true }
);

const usuariosConectados = {};

io.on('connection', socket => {

  const { usuario_id } = socket.handshake.query;

  usuariosConectados[usuario_id] = socket.id;


});

app.use((req, res, next)=>{
  req.io = io;
  req.usuariosConectados = usuariosConectados;

  return next();
})


app.use(cors());
app.use(express.json());
app.use('/files', express.static(path.resolve(__dirname, '..','uploads')));


app.use(routes);

server.listen(process.env.PORT);

