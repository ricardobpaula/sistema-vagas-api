const mongoose = require('mongoose');

const VagaSchema = new mongoose.Schema({
 imagem: String,
 empresa: String,
 valor: Number,
 tecnologias: [String],
 usuario: {
   type: mongoose.Schema.Types.ObjectId,
   ref: 'Usuario'
 }
},{
  toJSON: {
    virtuals: true,
  }
});

VagaSchema.virtual('imagem_url').get(function(){
  return `http://${process.env.API_URL}:${process.env.API_PORT}/files/${this.imagem}`
});

module.exports = mongoose.model('Vaga', VagaSchema);