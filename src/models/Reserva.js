const mongoose = require('mongoose');

const ReservaSchema = new mongoose.Schema({
 data: String,
 aprovada: Boolean,
 usuario: {
  type: mongoose.Schema.Types.ObjectId,
  ref: 'Usuario'
 },
 vaga: {
  type: mongoose.Schema.Types.ObjectId,
  ref: 'Vaga'
 }
});

module.exports = mongoose.model('Reserva', ReservaSchema);