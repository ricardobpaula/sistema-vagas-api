const express = require('express');
const multer = require('multer');
const uploadConfig = require('./config/upload');
const fs = require('fs');

const UsuariosController = require('./controllers/UsuariosController');
const DashboardController = require('./controllers/DashboardController');
const ReservasController = require('./controllers/ReservasController');
const VagasController = require('./controllers/VagasController');
const AprovacoesController = require('./controllers/AprovacoesController');
const RejeicoesController = require('./controllers/RejeicoesController');

const routes = express.Router();
const upload = multer(uploadConfig);

routes.get('/',(req, res)=>{
  return res.json({message: 'Bem Vindo'});
});

routes.post('/usuarios', UsuariosController.gravar);
routes.post('/vagas', upload.single('imagem'),VagasController.gravar);
routes.get('/vagas', VagasController.listar);

routes.get('/dashboard', DashboardController.exibir);

routes.post('/vagas/:vaga_id/reservas', ReservasController.gravar);

routes.post('/reservas/:reserva_id/aprovacoes', AprovacoesController.gravar);
routes.post('/reservas/:reserva_id/rejeicoes', RejeicoesController.gravar);


routes.get('/teste',(req,res) => {
  fs.unlink('./uploads/pp-1570103636159.jpg', (error) =>{
    if(error) throw error;
    console.log('Sucesso');
  }); 

});

module.exports = routes;