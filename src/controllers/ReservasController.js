const Reserva = require('../models/Reserva');

module.exports = {
  async gravar(req, res) {
    const { usuario_id } = req.headers;
    const { vaga_id } = req.params;
    const { data } = req.body;

    const reserva = await Reserva.create({
      usuario: usuario_id,
      vaga: vaga_id,
      data,
    });

    await reserva.populate('usuario').populate('vaga').execPopulate();

    const ownerSocket = req.usuariosConectados[reserva.vaga.usuario];

    if(ownerSocket){
      req.io.to(ownerSocket).emit('reserva_request', reserva);
    }

    return res.json(reserva);
  }
}