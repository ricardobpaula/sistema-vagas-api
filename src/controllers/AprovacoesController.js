const Reserva = require ('../models/Reserva');

module.exports = {
  async gravar(req, res) {
    const { reserva_id } = req.params;

    const reserva = await Reserva.findById(reserva_id).populate('vaga');

    reserva.aprovada = true;

    const reservaUsuarioSocket = req.usuariosConectados[reserva.usuario];

    if(reservaUsuarioSocket){
      req.io.to(reservaUsuarioSocket).emit('reserva_response', reserva);
    }

    await reserva.save();

    return res.json({send: true}); 
  }
}