const Vaga = require('../models/Vaga');

module.exports = {
  async exibir(req, res){
    const { usuario_id } = req.headers;

    const vagas = await Vaga.find({usuario: usuario_id });
   
    return res.json(vagas);
  }

}