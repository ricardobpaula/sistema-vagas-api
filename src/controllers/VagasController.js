const Vaga = require('../models/Vaga');
const Usuario = require('../models/Usuario');

module.exports = {

  async listar(req, res){
    const { tecnologia }  = req.query;

    const vagas = await Vaga.find({ tecnologias: tecnologia });

      return res.json(vagas);

  },
  
  async gravar(req, res){
    const { filename } = req.file;
    const { empresa, tecnologias, valor } = req.body;
    const { usuario_id } = req.headers;

    const usuario = await Usuario.findById(usuario_id);

    if(!usuario){
      return res.status(400).json({ error: 'Usuario não encontrado'});
    }

    const vaga = await Vaga.create({
      usuario: usuario_id, 
      imagem: filename, 
      empresa: empresa,
      tecnologias: tecnologias.split(',').map(tech => tech.trim()),
      valor: valor
    });
    


    return res.json({vaga});

  }
}